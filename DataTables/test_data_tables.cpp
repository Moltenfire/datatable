#include "pch.h"
#include "DataTables.h"


class TestDataTable : public ::testing::Test {
public:

  const std::string csvData0 = ",Molecule Solubility,Molecular Weight\nParacetamol,4.97,151\nCaffeine,5.05,194\nIndomethacin,0.4,358\nTrimethoprim,3.14,290";
  const std::string csvData1 = ",Molecule Solubility,Molecular Weight\nDrug X,1.11,45";
  const std::string csvData2 = ",Reference Code\nParacetamol,PAR\nCaffeine,CAF\nIndomethacin,IND\nTrimethoprim,TRI";
  const std::string csvData3 = ",Animal,Age\nAnne,Cat,3\nBob,Dog,7.5\nEve,Hamster,0.87\nSteve,Shark,105";
  const std::string csvData4 = ",Molecule Solubility\nParacetamol,4.97\nIndomethacin,0.4";

  DataTable table0{ csvData0 };
  DataTable table1{ csvData1 };
  DataTable table2{ csvData2 };
  DataTable table3{ csvData3 };
  DataTable table4{ csvData4 };
};


TEST_F(TestDataTable, test_construction_with_csv) {
}


TEST_F(TestDataTable, test_construction_of_empty_table) {
  DataTable emptyTable;
}

TEST_F(TestDataTable, test_contains_data) {
  EXPECT_TRUE(table0.constains("Paracetamol", "Molecule Solubility"));
  EXPECT_TRUE(table0.constains("Paracetamol", "Molecular Weight"));
  EXPECT_TRUE(table0.constains("Caffeine", "Molecule Solubility"));
  EXPECT_TRUE(table0.constains("Caffeine", "Molecular Weight"));
  EXPECT_TRUE(table0.constains("Indomethacin", "Molecule Solubility"));
  EXPECT_TRUE(table0.constains("Indomethacin", "Molecular Weight"));
  EXPECT_TRUE(table0.constains("Trimethoprim", "Molecule Solubility"));
  EXPECT_TRUE(table0.constains("Trimethoprim", "Molecular Weight"));
}


TEST_F(TestDataTable, test_column_names) {
  EXPECT_EQ(table0.getColumnNames(), std::set<std::string>({"Molecule Solubility", "Molecular Weight"}));
}


TEST_F(TestDataTable, test_row_names) {
  EXPECT_EQ(table0.getRowNames(), std::set<std::string>({"Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim"}));
}


TEST_F(TestDataTable, test_size) {
  EXPECT_EQ(table0.size(), 8);
}


TEST_F(TestDataTable, test_size_with_null_items) {
  DataTable newTable = table0.getUnion(table3);
  EXPECT_EQ(newTable.size(), 32);
}


TEST_F(TestDataTable, test_total_items) {
  EXPECT_EQ(table0.totalItems(), 8);
}


TEST_F(TestDataTable, test_total_items_with_null_items) {
  DataTable newTable = table0.getUnion(table3);
  EXPECT_EQ(newTable.totalItems(), 16);
}


TEST_F(TestDataTable, test_union_on_same_table) {
  DataTable newTable = table0.getUnion(table0);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim" }));
  EXPECT_EQ(newTable.totalItems(), 8);
}


TEST_F(TestDataTable, test_union_new_row) {
  DataTable newTable = table0.getUnion(table1);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim", "Drug X" }));
  EXPECT_EQ(newTable.totalItems(), 10);
}


TEST_F(TestDataTable, test_union_new_column) {
  DataTable newTable = table0.getUnion(table2);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight", "Reference Code" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim" }));
  EXPECT_EQ(newTable.totalItems(), 12);
}


TEST_F(TestDataTable, test_union_new_data) {
  DataTable newTable = table0.getUnion(table3);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight", "Animal", "Age" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim", "Anne", "Bob", "Eve", "Steve" }));
  EXPECT_EQ(newTable.totalItems(), 16);
}


TEST_F(TestDataTable, test_difference_on_same_table) {
  DataTable newTable = table0.getDifference(table0);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ }));
  EXPECT_EQ(newTable.totalItems(), 0);
}


TEST_F(TestDataTable, test_difference_with_no_overlap) {
  DataTable newTable = table0.getDifference(table3);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim" }));
  EXPECT_EQ(newTable.totalItems(), 8);
}


TEST_F(TestDataTable, test_difference_with_some_overlap) {
  DataTable newTable = table0.getDifference(table4);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecular Weight" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Caffeine", "Trimethoprim" }));
  EXPECT_EQ(newTable.totalItems(), 2);
}


TEST_F(TestDataTable, test_intersection_on_same_table) {
  DataTable newTable = table0.getIntersection(table0);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility", "Molecular Weight" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Caffeine", "Indomethacin", "Trimethoprim" }));
  EXPECT_EQ(newTable.totalItems(), 8);
}


TEST_F(TestDataTable, test_intersection_with_no_overlap) {
  DataTable newTable = table0.getIntersection(table3);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ }));
  EXPECT_EQ(newTable.totalItems(), 0);
}


TEST_F(TestDataTable, test_intersection_with_some_overlap) {
  DataTable newTable = table0.getIntersection(table4);

  EXPECT_EQ(newTable.getColumnNames(), std::set<std::string>({ "Molecule Solubility" }));
  EXPECT_EQ(newTable.getRowNames(), std::set<std::string>({ "Paracetamol", "Indomethacin" }));
  EXPECT_EQ(newTable.totalItems(), 2);
}
