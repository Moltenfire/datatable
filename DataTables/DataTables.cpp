#include "pch.h"
#include "DataTables.h"

#include <algorithm>
#include <iterator>
#include <set>
#include <sstream>

std::vector<std::string> splitString(const std::string& str, const char separator)
{
  std::vector<std::string> result;
  std::stringstream ss(str);

  for (std::string line; std::getline(ss, line, separator);) {
    result.push_back(line);
  }

  return result;
}


std::vector<std::vector<std::string>> parseCSV(const std::string& csv)
{
  std::vector<std::vector<std::string>> result;
  std::vector<std::string> lines = splitString(csv, '\n');

  for (const std::string& line : lines) {
    result.emplace_back(splitString(line, ','));
  }

  return result;
}


std::unique_ptr<Value> strToValue(const std::string& str)
{
  if (str.size() != 0) {
    char* end;
    double result = strtod(str.c_str(), &end);

    if (!(end == str.c_str() || *end != '\0')) {
      return std::make_unique<NumericValue>(result);
    }
  }

  return std::make_unique<StringValue>(str);
}


DataTable::DataTable(const std::string& csv)
{
  std::vector<std::vector<std::string>> csvData = parseCSV(csv);  
  std::vector<std::string> columnNames = csvData.front();
  columnNames.erase(columnNames.begin());
  csvData.erase(csvData.begin());
  
  for (const std::vector<std::string>& row : csvData) {
    const std::string& rowName = row.front();
    for (const std::string& columnName : columnNames) {
      for (auto it = row.begin() + 1; it != row.end(); ++it) {
        addValue(rowName, columnName, std::move(strToValue(*it)));
      }
    }
  }
}

void DataTable::addValue(const std::string& rowName, const std::string& columnName, std::unique_ptr<Value> value)
{
  m_data[std::make_pair(rowName, columnName)] = std::move(value);
  m_columnNames.insert(columnName);
  m_rowNames.insert(rowName);
}

bool DataTable::constains(const std::string& rowName, const std::string& columnName) const
{
  return m_data.find(std::make_pair(rowName, columnName)) != m_data.end();
}

const std::set<std::string>& DataTable::getColumnNames() const
{
  return m_columnNames;
}

const std::set<std::string>& DataTable::getRowNames() const
{
  return m_rowNames;
}

std::size_t DataTable::size() const
{
  return m_columnNames.size() * m_rowNames.size();
}

std::size_t DataTable::totalItems() const
{
  return m_data.size();
}

DataTable DataTable::getUnion(const DataTable& table) const
{
  DataTable newTable;

  for (const auto& item : m_data) {
    newTable.addValue(item.first.first, item.first.second, item.second->clone());
  }  
  for (const auto& item : table.m_data) {
    newTable.addValue(item.first.first, item.first.second, item.second->clone());
  }

  return newTable;
}

DataTable DataTable::getDifference(const DataTable & table) const
{
  DataTable newTable;

  std::set<std::string> rowNames;
  std::set<std::string> columnNames;
  std::set_difference(getRowNames().begin(), getRowNames().end(), table.getRowNames().begin(), table.getRowNames().end(), std::inserter(rowNames, rowNames.end()));
  std::set_difference(getColumnNames().begin(), getColumnNames().end(), table.getColumnNames().begin(), table.getColumnNames().end(), std::inserter(columnNames, columnNames.end()));

  for (const auto& item : m_data) {
    const std::string& row = item.first.first;
    const std::string& column = item.first.second;
    if ((rowNames.find(row) != rowNames.end()) && (columnNames.find(column) != columnNames.end())) {
      newTable.addValue(item.first.first, item.first.second, item.second->clone());
    }
  }

  return newTable;
}

DataTable DataTable::getIntersection(const DataTable & table) const
{
  DataTable newTable;

  std::set<std::string> rowNames;
  std::set<std::string> columnNames;
  std::set_intersection(getRowNames().begin(), getRowNames().end(), table.getRowNames().begin(), table.getRowNames().end(), std::inserter(rowNames, rowNames.end()));
  std::set_intersection(getColumnNames().begin(), getColumnNames().end(), table.getColumnNames().begin(), table.getColumnNames().end(), std::inserter(columnNames, columnNames.end()));

  for (const auto& item : m_data) {
    const std::string& row = item.first.first;
    const std::string& column = item.first.second;
    if ((rowNames.find(row) != rowNames.end()) && (columnNames.find(column) != columnNames.end())) {
      newTable.addValue(item.first.first, item.first.second, item.second->clone());
    }
  }

  return newTable;
}
