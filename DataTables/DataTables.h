#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>


class Value
{
public:
  virtual ~Value() = 0 {};
  virtual std::unique_ptr<Value> clone() const = 0;
};

class NumericValue : public Value
{
public:
  NumericValue(double value)
    : m_value(value)
  {}

  std::unique_ptr<Value> clone() const override {
    return std::make_unique<NumericValue>(m_value);
  }

private:

  double m_value;
};

class StringValue : public Value
{
public:
  StringValue(std::string value)
    : m_value(value)
  {}

  std::unique_ptr<Value> clone() const override {
    return std::make_unique<StringValue>(m_value);
  }

private:

  std::string m_value;
};

class DataTable
{
public:

  DataTable() = default;
  DataTable(const std::string& csv);

  void addValue(const std::string& rowName, const std::string& columnName, std::unique_ptr<Value> value);

  // Adds a new column and will use a form of union that only adds columns if the rows already exist
  // void addColumn(const std::string& rowName, const DataTable&);

  bool constains(const std::string& rowName, const std::string& columnName) const;

  // This returns true if rowName & columnName are in the table but there is no value
  // bool isNull(const std::string& rowName, const std::string& columnName) const;

  const std::set<std::string>& getColumnNames() const;
  const std::set<std::string>& getRowNames() const;
  std::size_t size() const;
  std::size_t totalItems() const;

  DataTable getUnion(const DataTable& table) const;
  DataTable getDifference(const DataTable& table) const;
  DataTable getIntersection(const DataTable& table) const;

private:

  std::set<std::string> m_columnNames;
  std::set<std::string> m_rowNames;
  
  // Maps (row names, column name) -> value
  std::map<std::pair<std::string, std::string>, std::unique_ptr<Value>> m_data;
};

